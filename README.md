This software is intended to control the color LED-stripes in the Stratum 0 Holodeck.

# LIGHT SETUP
The LED-light is divided into four sections. Two on each side. The front section is two thirds of the length, the back section is obviously one third.

Looking to the screen-side of the room:
* Section one is on the back on the left
* The folling sections are namend counter-clockwise 

# USEAGE
The software exports a rudimentary web-based interface. The interface is divided into two sections:
* A section to set colors or presets
* A section to list and add/alter presets

## Setting a color
A color is set using the following call:
```
http://holodeck.stratum0.net/holodeck/<section>/<colorCode>
```

Where:
* `<section>` is `all` or a section number `[1 .. 4]`
* `<color>` is a color-code

Color Codes are made up of a list of float values in the range of `[0.0 .. 1.0]`. Depending on the number of elements the color codes are iterpreted to be in different color spaces:
* `<float>`: Is interpreted as brightness and creates a RGBW-mixed broad-spectrum white in the given brightness 
* `<float>,<float>,<float>`: Is interpreted as RGB-value. The value is transformed into the RGBW color space before outputting it. Thus even a 1.0,0.0,0.0 (red) will contain some white.
* `<float>,<float>,<float>,<float>`: Is interpreted as RGBW-value. This value is directly outputted to the LEDs.
* `<float>,<float>,<float>,<float><float>,<float>,<float>,<float><float>,<float>,<float>,<float><float>,<float>,<float>,<float>` (16 floats): This special form can be used on the section code `all`. It is interpreted as four RGBW-color codes for the four sections. Thus with this color code a new color for all sections can be set 

## Preset-types:
There are two types of presets:
* System-presets: These presets contain all RGB-colors defined with names in CSS. These presets can not be altered. But user-presets have a highter priority.
* User-presets: These presets can be set by the user. The list has a limited length.

## Listing presets:
Presets can be listed using the following call:
```
http://holodeck.stratum0.net/preset 
```

## Setting presets:
A preset can be created or altered using the following call:
```
http://holodeck.stratum0.net/preset/<name>/<color>
```
## Using a preset:
A preset is set with the folowing call:
```
http://holodeck.stratum0.net/holodeck/<section>/<presetName>
```

If a preset with colors for all four sections is applied on a section other than `all` only the first RGBW-value will be used on the section.

