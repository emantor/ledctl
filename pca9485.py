#!/usr/bin/env python2

from math import ceil
from time import sleep
import struct
import smbus

class Pca:
  dc = [0.0]*16

  _i2cBus = None
  _i2cAddr = None

  _pca_mod0 = 0x00
  _pca_mod0_Sleep = 0x10
  _pca_mod0_noSleep = 0x00
  _pca_mod0_Autoincrement = 0x20
  _pca_mod0_noAutoincrement = 0x00

  _pca_mod1 = 0x01
  _pca_mod1_Invrt = 0x10
  _pca_mod1_noInvrt = 0x00
  _pca_mod1_totemPole = 0x04
  _pca_mod1_openDrain = 0x00

  _pca_prescaler = 0xFE
  _pca_prescaler_Clock = 25e6

  _pca_ledOffset = 0x06

  def _i2cDo(self, reg, payload):
    #for some strange reason i2c-access sometimes failes with an IOError for no reason
    #as a workaround we re-issue our request in this case for a few times
    #(but after inserting error handling bus communication worked much better. so it seems its a timing issue.)
    #TODO: somebody may electronically debug the i2c-bus
    success = False
    timeout = 10
    while not success and timeout != 0:
      timeout -= 1
      try:
        self._bus.write_i2c_block_data(self._i2cAddr, reg, payload)
        success = True
      except IOError:
        print "."
    if timeout == 0:
      raise IOError

  def setDC(self, channel, dc):
    self.setDCs(channel, [dc])

  def setDCs(self, startChannel, dcs):
    for i in range(len(dcs)):
      if dcs[i] is not None:
        self.dc[startChannel+i] = dcs[i]
    self._update()

  def fadeDCs(self, startChannel, dcs, seconds):
    STEP = 0.02
    steps = int(seconds/STEP)
    start = self.dc[startChannel:startChannel+len(dcs)]
    for i in range(1,steps+1):
      values = []
      for j in range(len(dcs)):
        if dcs[j] is None:
          values.append(start[j])
        else:
          # start + ((dcs-start)/steps)*i
          values.append(start[j] + ((dcs[j]-start[j])/steps)*i)
      self.setDCs(startChannel, values)
      sleep(STEP)

  def _update(self):
    bytes = []
    onoffset = 4096/len(self.dc)
    for i in range(len(self.dc)):
      #TODO better distribution of dcs
      dcoff = self.dc[i]
      bytes += [ord(x) for x in struct.pack("<H", (onoffset*i)&0xFFF)]
      rat = int(4095*dcoff)
      bytes += [ord(x) for x in struct.pack("<H", (onoffset*i + rat)&0xFFF)]
    limit = int(ceil(len(bytes)/32.0))
    for i in range(limit):
      self._i2cDo(self._pca_ledOffset+32*i, bytes[i*32:i*32+32])
      #self._bus.write_i2c_block_data(self._i2cAddr, self._pca_ledOffset+32*i, bytes[i*32:i*32+32])

  def __init__(self, i2cBus = 0, i2cAddr = 0x40, freq = 1e3):
    self._i2cAddr = i2cAddr
    self._bus = smbus.SMBus(i2cBus)

    self._i2cDo(self._pca_prescaler, [int(self._pca_prescaler_Clock/4096.0/freq-1)])
    self._i2cDo(self._pca_mod1, [self._pca_mod1_Invrt | self._pca_mod1_totemPole])
    self._i2cDo(self._pca_mod0, [self._pca_mod0_noSleep | self._pca_mod0_Autoincrement])

