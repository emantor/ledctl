from pca9485 import Pca
from time import sleep

p = Pca()

a = 1.0
fade = 0.3
interval = 0.5
while 1:
  p.fadeDCs(0,[a,0,0,0]*3,fade)
  sleep(interval)
  p.fadeDCs(0,[0,a,0,0,0,0,a,0,0,0,0,a],fade)
  sleep(interval)
  p.fadeDCs(0,[a,0,0,0]*3,fade)
  sleep(interval)
  p.fadeDCs(0,[0,0,a,0,0,0,0,a,0,a,0,0],fade)
  sleep(interval)
  p.fadeDCs(0,[a,0,0,0]*3,fade)
  sleep(interval)
  p.fadeDCs(0,[0,0,0,a,0,a,0,0,0,0,a,0],fade)
  sleep(interval)


