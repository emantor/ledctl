from pca9485 import Pca

p = Pca()

STEP = 0.01
vals = [0.0,.66,-.66]
while 1:
  p.setDCs(0,[abs(vals[0])/2]*4+[abs(vals[1])/2]*4+[abs(vals[2])/2]*4)
  for i in range(len(vals)):
    vals[i] = vals[i]+STEP
    if vals[i] > 1:
      vals[i] = -1.0

